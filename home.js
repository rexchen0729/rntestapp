import React from 'react';
import { Alert, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
// import {NativeModules} from 'react-native';
import RNAiqua, { AiquaConfigType } from 'heyaiquareactnativeplugin';

// const { RNAiquaSdk } = NativeModules;

export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      btnText: 'Default',
      viewColor: '#7FDCBA'
    };
  }

  componentDidMount(props) {
    RNAiqua.getPersonalizedConfig({
        key: 'navigateBtn',
        type: AiquaConfigType.TEXT,
        component: this.constructor.name,
        defaultValue: 'Navigate!'
    }).then(value => {
      console.log('got the response:', value);
      this.setState({btnText: value});
    }).catch(error => {

    });
  }

  static navigationOptions = {
    title: 'Welcome Home',
  };

  _onPressButton = () => {
    Alert.alert('You tapped the button!');

    RNAiqua.logEvent('rn-event-param', {'name': 'shiv', 'email': 'shiv@appier.com'});
  };

  _onPressNext = () => {
    
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
        <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
            <View style={styles.btn}>
              <Text style={styles.btnText}>
                {this.state.btnText}
              </Text>
            </View>
          </TouchableOpacity>
        <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
        <View>
          <TouchableOpacity onPress={this._onPressButton}>
            <View style={styles.btn}>
              <Text style={styles.btnText}>Click Me!</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  btn: {
    marginBottom: 30,
    width: 200,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2196F3'
  },
  btnText: {
    color: '#fff'
  } 
});

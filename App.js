/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

//import React from 'react';
import React, { Component } from "react";
//import { View, Text } from "react-native";
//import { createStackNavigator, createAppContainer } from 'react-navigation';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import HomeScreen from './home';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import RNAiqua from 'heyaiquareactnativeplugin';

class App extends Component {

  constructor(props) {
    super(props);
    this.initialiseSDK();
  }

  initialiseSDK() {

    RNAiqua.configure({
      appId: '9cce3dd2bb98c0dad844', //'bda55ee01e48ea9d4a91',
      appGroup: 'group.com.appier.AIQUA.notification',
      isDev: true // ios dev or prod - default `true`
    });
    RNAiqua.setName('Rex');
    
  }

  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          height: 100,
          padding: 20
        }}
      >
        <View style={{ backgroundColor: "blue", flex: 0.3 }} />
        <View style={{ backgroundColor: "red", flex: 0.5 }} />
        <Text>Hello World!</Text>
      </View>
    );
  }
}

export default App;
